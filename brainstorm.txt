IMMEDIATE TODOS:
* Deontes completed - also, one meta TODO: use bitbucket tickets instead of a .txt file!

Make repo *
Share repo *
Decide on DB
Decide on structure *

What this app should do:


Freewriting:

Structure of the app - probably going to go with something similar to previous app - express + angular, client + server folders to separate both parts. Going to forego authentication unless there is some time at the end, so don't burn that bridge alltogether - now what to do about the database - need something that's easily portable to another system. I don't have a lot of experience with NoSQL at this point, so probably better to go with something that will set up quickly on another
machine - thinking of sqlite. I know, it's a sin to include "large" or non logic files in a repo, but for this instance it may be well suited. Just a sqlite file with whatever table structure would work.

UI - things to learn:
"uploading" files


Major APIS:
Upload file <- Nope, didn't read instructions. A file will be a row in the DB - no uploading.
Get file information
Query text from file (depends on DB structure; not fixed yet)

So, on the second lookover here:
***By "a method" I mean URL endpoint, eg. /api/getFileData***
Need a method to get metadata abut files (How many, names, things like that)
Need a method to get further information about a single file
Need a method to add a row (file) to the database

Immediate priorities: 
Create scaffolding (yeoman?) DONE
Yeoman for angular, probably just use express from scratch. We'll see. DONE
Work on CLIENT SIDE - for now, this means the main controller (and maybe other stubs) and the "wishlist" angular service DONE
Need to clean up variable names at the end so they make more sense
Now on to the editing part
Need another table for the tags! (I think)

Thigs that would be cool: (ie. stuff to do after it's done)
Showing nearby documents (with gju + latlon row / table, would be pretty simple)



