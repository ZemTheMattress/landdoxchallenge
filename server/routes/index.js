var express = require('express');
var router = express.Router();
var path = require('path');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/views/:name', function(req, res) {
  res.sendfile(path.resolve(__dirname + './../public/views/' + request.params.name));
});

module.exports = router;
