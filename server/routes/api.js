var express = require('express');
var router = express.Router();
var sqlite = require('sqlite3').verbose();
var path = require('path');
var squel = require('squel');

var dbPath = path.resolve(__dirname, './../LandDocks');

var db = new sqlite.Database(dbPath, function() {
  console.log('connected to database at', dbPath);
});

router.get('/getDocMetadata', function(req, /*loggedIn,*/ res) {
  db.all('SELECT * From documents', function(err, rows) {
    if (err) {
      //(don't want to send too detailed response lest the rabble catch wind of the app's inner workings)
      res.status(500).send('Error getting document metadata');
    }
    else {
      if (rows.length > 0) {
        res.send(rows);
      }
      else {
        res.status(500).send('No documents in the database.');
      }
    }
  });
});

router.post('/addDoc', function(req, res) {
  //Verify here!!!!
  var query = squel.insert()
                .into('documents')
                .setFields({
                  documentType: req.body.docInfo.documentType,
                  fileName: req.body.docInfo.fileName,
                  notes: req.body.docInfo.notes,
                  active: req.body.docInfo.active,
                  path: '/'

                }).toString();
  db.run(query, function(err) {
    if (err) {
      console.log(err, query);
      res.status(500).send({error: 'Error inserting new doc'});
    }
    else {
      res.send(200);
    }
  });
});

module.exports = router;


//(The loggedIn function would go here, to be passed in to the api routes for security purposes)
