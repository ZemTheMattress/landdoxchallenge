### How do I get set up? ###

* Make sure you have node.js on your computer - I have version 10.38 for node, and 1.4.28 for npm.
* Change to the server folder and run 
```
#!javascript
npm install
```

* This will install the packages needed
* In the server folder, type 

```
#!javascript

node app.js
```

* This will start the server; that should be it! Enter localhost:3001 into the web browser, and the website will show up!




### Things that worked ###

* I think sqlite was the right database tool for this particular project
* Once everything was set up (More, uh, on that below...), development went pretty quickly. 

### Things that didn't work ###

* I took way too long to set up the environment - in this case, encountered npm installation errors for the client side dependencies. (grunt, etc.)
* Programming this fast, I wasn't confident with the names of my variables. It may seem petty, but these types of things can sometimes snowball, especially when having to remember the most logical variable names across modules (I'm looking at you, docMetadata)
* I feel like I didn't look at the directions enough, or planned out the database structure too well. (It's just one table.)
 
### Known Issues ###
* There is no verification of values for the document creation
* Though errors are handled, the user is not notified that something had gone wrong.