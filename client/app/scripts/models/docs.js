angular.module('landDocks')
  .service('docModel', [ 'docService', function() {
    var docModel = {};
    docService.getMetadata()
      .then(function(result) {
        docModel.docCount = result.length;
        docModel.docMetadata = result;
        //So we can list the items
        $docModel.columns = _.keys(result[0]);
        console.log($scope.columns, result);
      }, function(reason) {
        $scope.alerts.push({
          type: 'warning',
          msg: reason
        });
      });

  }]);
