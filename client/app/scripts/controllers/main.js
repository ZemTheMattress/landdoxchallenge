'use strict';
angular.module('landDocks')
  .controller('MainCtrl', ['$scope', 'docService', function($scope, docService ) {
    $scope.alerts = [];
    $scope.columns = [];
    $scope.docMetadata = {};
    $scope.newDoc = {};
    $scope.stats = {}

    getDocs();

    $scope.addDoc = function() {
      //API to add document
      //verify newDoc first
      docService.addDoc($scope.newDoc)
        .then(function() {
          getDocs();
        }, function(reason) {
          $scope.alerts.push({
            type: 'warning',
            msg: reason
          });
        });
    }

    function getDocs() {
      docService.getMetadata()
        .then(function(result) {
          $scope.stats.docCount = result.length;
          $scope.docMetadata = result;
          //So we can list the items
          $scope.columns = _.keys(result[0]);
          console.log($scope.columns, result);
        }, function(reason) {
          $scope.alerts.push({
            type: 'warning',
            msg: reason
          });
        });
    }
  }]);

