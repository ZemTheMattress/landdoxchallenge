angular.module('landDocks')
  .service('docService', ['$q', '$http', function($q, $http) {
    var docService = {};

    docService.getMetadata = function() {
      var deferred = $q.defer();

      $http.get('/api/getDocMetadata')
        .success(function(result) {
          //Maybe validate here
          deferred.resolve(result);
        })
        .error(function(reason) {
          deferred.reject(reason);
        });

      return deferred.promise;

    }

    docService.addDoc = function(docInfo) {
      //DocInfo will be an object with whataver we need to make a new doc.
      var deferred = $q.defer();

      $http.post('/api/addDoc', { docInfo: docInfo })
        .success(function() {
          deferred.resolve();
        })
        .error(function() {
          deferred.reject();
        });

        return deferred.promise;
    }

    return docService;

  }]);
